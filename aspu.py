#!/usr/bin/env python3

from datetime import datetime
from bs4 import BeautifulSoup
import requests
import locale

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

class Aspu():
    def __init__(self):
        self.base_url = "https://aspu.org/planning_all.php?action=&year="

    def get_date(self, date_str):
        # <from> <day> <month> <year> <to> <day> <month> <year>
        # Remove some useless data
        date = date_str.split()[1:]
        del date[3] 

        start = '{} {} {}'.format(date[0], date[1], date[2])
        start = datetime.strptime(start, '%d %B %Y')

        end = '20 {} {} {}'.format(date[3], date[4], date[5])
        end = datetime.strptime(end, '%H %d %B %Y')

        dates = {'start': start, 'end': end}
        return dates


    def get_dates(self, year=datetime.now().year):
        self.url = self.base_url + str(year)
        
        r = requests.get(self.url)
        soup = BeautifulSoup(r.text, "lxml")

        activities = []
        for tr in soup.find_all('tr'):
            children = tr.findChildren()
            if len(children) == 1: # Subtitle: date
                continue # Date is repeated in each <td>

            dates = self.get_date(children[1].get_text())
            dates['activity'] = children[4].get_text()
            activities.append(dates)
            
        return activities


if __name__ == "__main__":
    aspu = Aspu()
    activities = aspu.get_dates()
    print(activities)
