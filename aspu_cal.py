#!/usr/bin/env python3

from icalendar import Calendar, Event, vText
from aspu import Aspu

class CalAspu():
    def __init__(self):
        self.cal = Calendar()

    def make_cal(self, activities):
        for i, week_end in enumerate(activities):
            event = Event()
            event.add('summary', 'ASPU — {}'.format(week_end['activity']))
            event.add('dtstart', week_end['start'])
            event.add('dtend', week_end['end'])
            event.add('location', 'Maubeuge')
            event.add('uid', i)

            self.cal.add_component(event)

    def write_cal(self, path='./aspu_cal.ics'):
        f = open(path, 'wb')
        f.write(self.cal.to_ical())
        f.close()


if __name__ == '__main__':
    aspu = Aspu()
    cal = CalAspu()

    activities = aspu.get_dates()

    cal.make_cal(activities)
    cal.write_cal()
